

tol=1e-3

def version():
    return '0.0.1' #update in setup.py

class Params(object):
    VertInfill=0
    HorInfill=1
    CrossInfill=2
    NoInfill=3
    
def pdist(p1,p2):
    return ((p1[0]-p2[0])**2+(p1[1]-p2[1])**2)**.5
