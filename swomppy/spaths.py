import pyx
pyx.unit.set(defaultunit='mm')
import numpy as np
from copy import deepcopy

from .swomppy import tol,pdist

#returns pyx length in mm
def inmm(length,lentype='pyxlength'):
    def conv(L):
        if lentype=='pyxlength':
            pass
        else:
            L = pyx.unit.length(L,unit=lentype)
        return pyx.unit.tomm(L)

    if type(length) is list:
        return [conv(L) for L in length]
    elif type(length) is tuple:
        return tuple([conv(L) for L in length])
    else:
        return pyx.unit.tomm(length)


class sline(pyx.normpath.normline_pt):
    
    def __init__(self, x0_pt, y0_pt, x1_pt, y1_pt,attrs=None):
        pyx.normpath.normline_pt.__init__(self, x0_pt, y0_pt, x1_pt, y1_pt)
        if not attrs:
            attrs = {}
        assert type(attrs) is dict, 'attrs for an scurve must be a dictionary'
        self.attrs=attrs
        
    def __str__(self):
        return "sline[start="+str(self.start())+",end="+str(self.end())+",attrs="+str(self.attrs)+"]"
        
    def start(self):
        return inmm(self.at_pt([0])[0],lentype='pt')
    
    def end(self):
        return inmm(self.at_pt([1])[0],lentype='pt')
    
    def reversed(self):
        rline = pyx.normpath.normline_pt.reversed(self)
        return sline(rline.x0_pt, rline.y0_pt, rline.x1_pt, rline.y1_pt,deepcopy(self.attrs))
    
    def length(self):
        return pdist(self.start(),self.end())

class scurve(pyx.normpath.normcurve_pt):

    def __init__(self, x0_pt, y0_pt, x1_pt, y1_pt, x2_pt, y2_pt, x3_pt, y3_pt,attrs=None):
        pyx.normpath.normcurve_pt.__init__(self, x0_pt, y0_pt, x1_pt, y1_pt, x2_pt, y2_pt, x3_pt, y3_pt)
        if not attrs:
            attrs = {}
        assert type(attrs) is dict, 'attrs for an scurve must be a dictionary'
        self.attrs=attrs
    
    def __str__(self):
        return "sline[start="+str(self.start())+",end="+str(self.end())+",cp1="+str(self.cp1())+",cp2="+str(self.cp2())+",attrs="+str(self.attrs)+"]"
    
    def start(self):
        return inmm(self.at_pt([0])[0],lentype='pt')
    
    def end(self):
        return inmm(self.at_pt([1])[0],lentype='pt')
    
    def cp1(self):
        return inmm((self.x1_pt,self.y1_pt),lentype='pt')
        
    def cp2(self):
        return inmm((self.x2_pt,self.y2_pt),lentype='pt')
    
    def reversed(self):
        rcurve = pyx.normpath.normcurve_pt.reversed(self)
        return scurve(rcurve.x0_pt, rcurve.y0_pt, rcurve.x1_pt, rcurve.y1_pt, rcurve.x2_pt, rcurve.y2_pt, rcurve.x3_pt, rcurve.y3_pt,deepcopy(self.attrs))
    
    def length(self):
        #don't use arclen_pt as it give the wrong answer!
        points = [inmm(pt,lentype='pt') for pt in self.at_pt([.01*n for n in range(101)])]
        length =0
        for p1,p2 in zip(points[0:-1],points[1:]):
            length+=pdist(p1,p2)
        return length
    
    def xypoints(self,sep=.2):
        N = int(self.length()//sep+2)
        step=1.0/float(N-1)
        pts = [step*n for n in range(N)]
        return [inmm(pt,lentype='pt') for pt in self.at_pt(pts)]


class spath:
    #A swomppy path is a list of modified PyX normsubpath items that can be used in a normpath for PyX operations
    
    def __init__(self,inpoints,closed=False,attrs=None):
        self.attrs=attrs
        points = deepcopy(inpoints)
        assert len(points)>1, "Must have at least two points"
        if type(points) is list:
            start = points.pop(0)
            lastpoint = point = start
            assert len(start)==2, "First item when creating an spath should be a tuple of (x,y) coordinates"
            cpath = pyx.path.path(pyx.path.moveto(*start))
            for point in points:
                if len(point)==2:
                    if pdist(lastpoint,point) > .05:
                        #normal point
                        cpath.append(pyx.path.lineto(*point))
                        lastpoint = point
                elif len(point)==3 and len(point[0]) == 2:
                    #Curve to point
                    cpath.append(pyx.path.curveto(point[0][0],point[0][1],point[1][0],point[1][1],point[2][0],point[2][1]))
                
            if closed:
                cpath.append(pyx.path.closepath())
            norm = cpath.normpath()
            nitems = norm[0].normsubpathitems
            #Length can be zero if only two points and they are very close
            if len(nitems) == 0:
                cpath.append(pyx.path.lineto(*points[0]))
                norm = cpath.normpath()
                nitems = norm[0].normsubpathitems
        elif type(points) is pyx.path.normsubpath:
            assert len(points)>0, "Must have at least one item"
            nitems = points.normsubpathitems
            
        else:
            assert False, "spath should be created from a list of points or a pyx normsubpath"
        
        self.items=[]
        for item in nitems:
            if type(item) is pyx.normpath.normline_pt:
                self.items.append(  sline( item.x0_pt, item.y0_pt, item.x1_pt, item.y1_pt  )  )
            if type(item) is pyx.normpath.normcurve_pt:
                self.items.append(  scurve( item.x0_pt, item.y0_pt, item.x1_pt, item.y1_pt, item.x2_pt, item.y2_pt, item.x3_pt, item.y3_pt  )  )
        
        self.update_normpaths()
    
    def __getitem__(self, i):
        return self.items[i]

    def __len__(self):
        return len(self.items)
    
    def update_normpaths(self):
        
        self.nsp = pyx.path.normsubpath(self.items,closed=self.closed())
        self.np = pyx.path.normpath([self.nsp])
        
    def as_normpath(self):
        return self.np

    def as_normsubpath(self):
        return self.nsp
    
    def start(self):
        try:
            return self[0].start()
        except:

            assert False
    
    def end(self):
        return self[-1].end()
    
    def arclen(self):
        return inmm(self.np.arclen())
    
    #returns bounding box
    def bbox(self):    
        t = pyx.unit.tomm(self.np.bbox().top())
        b = pyx.unit.tomm(self.np.bbox().bottom())
        l = pyx.unit.tomm(self.np.bbox().left())
        r = pyx.unit.tomm(self.np.bbox().right())
        return (t,b,l,r)
    
    def isClockwise(self):
        #Check path is clockwise
        #get bounding box
        t,b,l,r = self.bbox()
        boxbottom = spath([(l,b),(r,b)],False)
        #intersection of path with bottom bounding box is lowest point
        dists = self.intersectdist(boxbottom)
        cprod = 0
        n=0
        while abs(cprod)<1e-9:
            delta = 1e-3
            #find points either side of intersection
            x0,y0 = self.at(dists[n]-delta)
            x1,y1 = self.at(dists[n])
            x2,y2 = self.at(dists[n]+delta)
            #check handedness with cross product of vectores into and out of lowest point
            cprod = ((x1-x0)*(y2-y1)-(x2-x1)*(y1-y0))
            n+=1
        return cprod<0
    
    def reversed(self):
        rev = deepcopy(self)
        rev.items = [it.reversed() for it in rev[-1::-1]]
        rev.update_normpaths()
        return rev
    
    def at(self,dist):
        return spathlist(self).at(dist)
    
    def intersectdist(self,path):
        return spathlist(self).intersectdist(path)
    
    def nintersects(self,path):
        return spathlist(self).nintersects(path)
    
    def intersection(self,path):
        return spathlist(self).intersection(path)
    
    #Note this only works if there are corners where the rep path starts and ends
    def replaceSubpath(self,rep_path):
        r1x,r1y = rep_path.start()
        r2x,r2y = rep_path.end()
        founds = False
        e=None
        founde = False
        s=None
        for n,line in enumerate(self):
            mx,my = line.start()
            if ((mx-r1x)**2 + (my-r1y)**2)**.5 < tol:
                founds=True
                s=n
                if founde and founds:
                    break
            elif ((mx-r2x)**2 + (my-r2y)**2)**.5 < tol:
                founde=True
                e=n
                if founde and founds:
                    break
        assert (founde and founds), "Couldn't match subpath for replacement"
        if e-s == 1:
            self.items[s:e]=rep_path[:]
        elif s-e==1:
            rep_path = rep_path.reversed()
            self.items[s:e]= rep_path[:]
        else:
            assert False, "Subpath to replace doesn't fall on straight line."
        self.update_normpaths()
        
    def closed(self):
        return pdist(self.start(),self.end()) < tol

class spathlist:
    
    def __init__(self,paths=None):
        
        if paths is None:
            self.paths = []
        elif type(paths) is spath:
            self.paths = [deepcopy(paths)]
        elif type(paths) is list:
            for path in paths:
                assert type(path) is spath, "spathlist can only store spaths"
            self.paths=deepcopy(paths)
        elif type(paths) is pyx.path.normpath:
            self.paths=[]
            #loop through all normsubpaths in the normpath
            for nspath in paths:
                self.paths.append(spath(nspath))
        else:
            assert False, "spathlist should be initiated with a list of spaths, a single spath, or a pyx normpath"


    def __getitem__(self, i):
        return self.paths[i]

    def __len__(self):
        return len(self.paths)
    
    def append(self,elem):
        assert type(elem) is spath, "spathlist can only store spaths"
        self.paths.append(elem)
    
    def as_normpath(self):
        return pyx.path.normpath([pth.nsp for pth in self])
        
    def bbox(self):
        np=self.as_normpath()
        t = pyx.unit.tomm(np.bbox().top())
        b = pyx.unit.tomm(np.bbox().bottom())
        l = pyx.unit.tomm(np.bbox().left())
        r = pyx.unit.tomm(np.bbox().right())
        return (t,b,l,r)
    
    #x,y position at a distance dist along path
    def at(self,dist):
        dist = pyx.unit.length(dist,type='t', unit='mm')
        return inmm(self.as_normpath().at(dist))
    
    # distance of intersections along spathlist
    def intersectdist(self,path):
        normp = self.as_normpath()
        i1,i2 = normp.intersect(path.as_normpath())
        return inmm(normp.paramtoarclen(i1))
    
    #number of intersections with path:
    def nintersects(self,path):
        i1,i2 = self.as_normpath().intersect(path.as_normpath())
        return len(i1)
    
    # returns (x,y) coordinates of intersecton
    def intersection(self,path):
        normp = self.as_normpath()
        i1,i2 = normp.intersect(path.as_normpath())
        return ([inmm(normp.at(i)) for i in i1])
    
    def checkPathDirsValid(self,fix=False):
        depths = self.calPerimDepths()
        #if path deth is even it should be anti-clockwise, odd it should be clockwise
        valid=True
        if fix:
            fixpaths = []
        for (path,deep) in zip(self,depths):
            if path.isClockwise() == (deep%2==0):
                if fix:
                    fixpaths.append(path.reversed())
                valid=False
            else:
                if fix:
                    fixpaths.append(path)
        if fix and not valid:
            self.paths=fixpaths
        #valid is returned as false if a path was invalid, even if fixed
        return valid
    
    
    #calculated "depths" of perimeter, i.e. outside is zero, boundry of hole is 1, object inside hole is 2
    def calPerimDepths(self):
        npth = len(self)
        t,b,l,r = self.bbox()
        #Start as depths 0
        depths=[0]*npth
        #loop over all pairs
        for n in range(npth):
            for m in range(n+1,npth):
                #check path pairs don't intersect
                assert self[n].nintersects(self[m])==0, 'spathlist has overlapping spaths. Cannot calculate depths'
                xm,ym = self[m].start()
                #make a line from this point to outside the main bounding box
                lineout = spath([(xm,ym),(r+1,t+1)])
                #check intersection with nth normpath
                num_int = self[n].nintersects(lineout)
                #if number of intersections is odd then mth path is inside nth
                if num_int%2==1:
                    depths[m]+=1
                else:
                    #if mth is not inside nth do same to check for nth inside mth
                    xn,yn = self[n].start()
                    lineout = spath([(xn,yn),(r+1,t+1)])
                    num_int = self[m].nintersects(lineout)
                    if num_int%2==1:
                        depths[n]+=1
        return depths
    
    def insetPath(self,offset,CheckInset=True):
        
        # This checks if the subpaths are too small and would flip inside out on inset.
        # Bit of a hacky fix needed due to pyx bug
        if CheckInset:
            usepath = spathlist()
            depth = self.calPerimDepths()
            for (subpath,deep) in zip(self,depth):
                #if outsetting +ve outset of hole, or negative outset of object then skip for this subpath
                if (offset>0) == (deep%2==1):
                    usepath.append(subpath)
                else:
                    #calculate corner to corner length of bounding box
                    bb = subpath.bbox()
                    clen = ((bb[1]-bb[0])**2+(bb[3]-bb[2])**2)**.5
                    #compare corner length to offset and total path length of subpath
                    if clen>3*offset and subpath.arclen()>clen*2+3*offset:
                        usepath.append(subpath)
            if len(usepath)==0:
                return spathlist()
        else:
            usepath=self
        
        #this is a normpath
        offPathNP =  pyx.deformer.parallel(offset).deform(usepath.as_normpath())
        return spathlist(offPathNP)

    def generateInFill(self,spacing,vert=True):
        #Calculate bounding box
        t,b,l,r = self.bbox()
        #Start infill position, d, just outside box. should be modded with spacing so subsequent layers of infill overlap;
        #d is and x or a y position depending on if infill is vertical or horizontal
        if vert:
            d=l-1
            d-=d%spacing
            limit = r+1
        else:
            d=b-1
            d-=d%spacing
            limit = t+1
        
        filllines=spathlist()
        while d<=limit:
            #For each infill line generate a path longer than the bounding box
            if vert:
                line = spath([(d,t+1),(d,b-1)])
            else:
                line = spath([(l-1,d),(r+1,d)])
            ##Find intersects with shell
            
            intersections=[]
            for path in self:
                inters = line.intersection(path)
                for inter in inters:
                    intersections.append(inter)
            
            #sort intersections and remove pairs within tolerance
            if len(intersections)>1:
                #select coordinate to sort depending on vertical or horizontal
                if vert:
                    varcord = 1
                else:
                    varcord = 0
                
                #sort into order
                intersections.sort(key=lambda coord: coord[varcord])
                
                #Check neighboring pairs for being too close, happens at corner nue to pyx rounding errors for intersections
                a = np.asarray([i[varcord] for i in intersections])
                D =a[1:]-a[:-1]
                w = np.where(D>tol)
                pts = list(w[0]+1)
                pts.insert(0,0)
                #remove neighboring pairs
                intersections = [intersections[i] for i in pts]
                
            #We are intersecting closed paths from the outside to the outside so we should always have an even number of intersections
            num = len(intersections)
            assert num%2==0, 'Line should have even number of intersections. Aborted in confusion'
            #Generate infill lines
            if num==0:
                pass
            else:
                ptslist1 = list(range(0,num,2))
                ptslist2 = list(range(1,num,2))
                for n1,n2 in zip(ptslist1,ptslist2): 
                    filllines.append(spath([intersections[n1],intersections[n2]]))
            d+=spacing
        return filllines


