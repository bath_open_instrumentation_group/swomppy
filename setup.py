__author__ = 'Julian Stirling'

from setuptools import setup, find_packages

from os import path

here = path.abspath(path.dirname(__file__))


setup(name = 'swomppy',
      version = '0.0.1', #update in version.py
      description = 'Slicer With Optimised Mechanical Performance in Python',
      long_description = 'The purpose of this slicer is not to be an awesome general slicer, but to be able to make flexures with optimised mechanical performance',
      #url = '',
      author = 'Julian Stirling',
      author_email = 'julian@julianstirling.co.uk',
      #download_url = '',
      packages = find_packages(),
      keywords = ['3Dprinter','slicer'],
      zip_safe = True,
      classifiers = [
          'Development Status :: 2 - Pre-Alpha',
          'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
          'Programming Language :: Python :: 3.6'
          ],
      install_requires = [
          'numpy',
          'matplotlib',
          'pyx'
          ],
      )

